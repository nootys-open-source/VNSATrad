package fr.cyril;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    public static void main(String[] args)
    {

        List<String> entryScriptFile;
        List<String> entryTraductionFile;

        // Extractor
        List<String> entryExtractScriptFile;
        List<String> entryExtractedScriptFile;


        // File Extraction
        try
        {
            Path pathFileExtraction = Paths.get("D:\\visual-trad\\extraction\\input","file.txt");
            entryExtractScriptFile = Files.readAllLines(pathFileExtraction, Charset.forName("UTF-16"));
            System.out.println("Count Extractor Script: "+entryExtractScriptFile.size()+" line(s)");
            entryExtractedScriptFile = extractAllChains(entryExtractScriptFile);
            System.out.println("Count Extracted Script: "+entryExtractedScriptFile.size()+" line(s)");

            //Save file
            /*
            FileWriter writer = new FileWriter("extract-file.txt",true);
            for(String entry : entryExtractedScriptFile)
                writer.write(entry+"\n");*/

            BufferedWriter writer = new BufferedWriter(new FileWriter("extract-file.txt"));
            for(String entry : entryExtractedScriptFile)
                writer.write(entry+"\n");

            writer.close();

        } catch (IOException e)
        {
            e.printStackTrace();
        }




        Path pathEntry = Paths.get("D:\\visual-trad\\scripts","g01.ks");
        Path pathTradEntry = Paths.get("D:\\visual-trad\\trad","sc.txt");


        try
        {
            entryScriptFile = Files.readAllLines(pathEntry, Charset.forName("UTF-16"));
            System.out.println("Count Script: "+entryScriptFile.size());

            //entryScriptFile = removeAllCommentaries(entryScriptFile);
            //System.out.println("Count Script without com: "+entryScriptFile.size());

            entryTraductionFile = Files.readAllLines(pathTradEntry);
            System.out.println("Count Trad: "+entryTraductionFile.size());

            /*
            entryTraductionFile = removeAllCommentaries(entryTraductionFile);
            System.out.println("Count Trad without com: "+entryTraductionFile.size());*/


            //setTraduction(entryScriptFile,entryTraductionFile);

            List<String> traducedFile = traduceFile(entryScriptFile,entryTraductionFile);

            if(traducedFile !=  null && !traducedFile.isEmpty())
            {
                BufferedWriter writer = new BufferedWriter(new FileWriter("traduced-file.txt"));
                for(String entry : traducedFile)
                writer.write(entry+"\n");

                writer.close();


                /*
                FileWriter writer = new FileWriter("traduced-file.txt");
                for(String entry : traducedFile)
                    writer.write(entry+"\n");*/

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /** *******************  File Chains Extractor ************************** */
    private static List<String> extractAllChains(List<String> chainToExtract)
    {
        System.out.println("extractAllChains extract");

        List<String> dataScriptWithoutCom = new ArrayList<>();

        for(int i = 0; i < chainToExtract.size();i++)
        {
            if( ( chainToExtract.get(i).length() >= 2  && !chainToExtract.get(i).substring(0,2).equals(";;")) && !chainToExtract.get(i).isEmpty() && ( chainToExtract.get(i).contains("[wvl]") || chainToExtract.get(i).contains("[np]")) )
                dataScriptWithoutCom.add(chainToExtract.get(i));
        }

        return dataScriptWithoutCom;

        // regex : (\[wvl\]|\[np\])$
    }

    private static List<String> extractAllChains(String chain)
    {
        System.out.println("Extraction");
        List<String> allMatches = new ArrayList<String>();

        Matcher m = Pattern.compile(".*(\\[wvl\\]|\\[np\\])$*").matcher(chain);

        while (m.find())
        {
            allMatches.add(m.group());
            System.out.println(m.group());
        }


        return allMatches;
        // regex : (\[wvl\]|\[np\])$
    }
    /* ********************************************************************** */


    private static List<String> removeAllCommentaries (List<String> dataScript)
    {
        // regex : ^;{2}.*

        // multiline  (?m)
        //remove lines (?:\r?\n)?

        List<String> dataScriptWithoutCom = new ArrayList<>();

        for(int i = 0; i < dataScript.size();i++)
        {
            if(dataScript.get(i).length() < 2 || ( dataScript.get(i).length() >= 2  && !dataScript.get(i).substring(0,2).equals(";;")) )
            dataScriptWithoutCom.add(dataScript.get(i));
        }

        return dataScriptWithoutCom;

       // return testChain.replaceAll("(?m)^;{2}.*(?:\r?\n)?","");


    }


    private static String removeAllCommentaries (String testChain)
    {
        // regex : ^;{2}.*

        // multiline  (?m)
        //remove lines (?:\r?\n)?

        return testChain.replaceAll("(?m)^;{2}.*(?:\r?\n)?","");


    }



    private static void setTraduction (List<String> script, List<String> traduction)
    {

        if(script.size() == traduction.size())
        {
            for(int i = 0; i < script.size(); i++)
            {
                System.out.println("Replacing : "+script.get(i)+"By "+traduction.get(i));
                script.set(i,traduction.get(i));
            }

        }

    }

    private static List<String> traduceFile (List<String> script, List<String> traduction)
    {
        List<String> scriptFileTraduit = new ArrayList<String>();

        Iterator<String> iterator  = traduction.iterator();

        for(String line : script)
        {
            if( ( line.length() >= 2  && !line.substring(0,2).equals(";;")) && ( line.contains("[wvl]") || line.contains("[np]")) ) {

                String traducedString = iterator.next();
                scriptFileTraduit.add(traducedString);
                System.out.println("Replacing : "+line+"By "+scriptFileTraduit.get(scriptFileTraduit.size()-1));
            }
            else
            {
                scriptFileTraduit.add(line);
                System.out.println("Not Replacing line : "+line);
            }



        }

        return scriptFileTraduit;
   }
}
